/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntornoGrafo;

import GraphLA.Vertex;
import javafx.geometry.Point2D;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 *
 * @author scarlet Espinoza
 */
public class VertexGrafico<E> extends StackPane {
    private static Point2D sizes = new Point2D(200, 50);
    Rectangle rVertex = new Rectangle(200,50);
    Text t = new Text();
    Vertex<E> vertex;
    
    public VertexGrafico(E data){
        vertex = new Vertex(data);
        t.setText(data.toString());
        t.setFill(Color.WHITE);
        t.setStyle("-fx-font-weight: bold");
        
        
        this.getChildren().addAll(rVertex,t);
    }

    public Rectangle getrVertex() {
        return rVertex;
    }

    public void setrVertex(Rectangle rVertex) {
        this.rVertex = rVertex;
    }
    
    public static double getCenter(){
        return sizes.getX()/2;
    }
    
    

}

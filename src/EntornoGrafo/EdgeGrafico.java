/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntornoGrafo;

import Entidades.Movie;
import GraphLA.Edge;
import GraphLA.Vertex;
import javafx.geometry.Point2D;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 *
 * @author scarlet Espinoza
 */
public class EdgeGrafico<E> extends StackPane {
    private static Point2D sizes = new Point2D(350, 50);
    Rectangle rEdge = new Rectangle(350,50);
    Text t = new Text();
    Vertex<E> vertexOrigen;
    Vertex<E> vertexDestino;
    Movie movie;
    Edge<E> edge;
    public EdgeGrafico(Vertex<E> vertexOrigen,Vertex<E> vertexDestino,Movie movie){
        edge = new Edge(vertexOrigen,vertexDestino,movie);
        t.setText(edge.getMovie().toString());
        t.setFill(Color.WHITE);
        t.setStyle("-fx-font-weight: bold");
        rEdge.setFill(javafx.scene.paint.Color.PURPLE);
        
        
        this.getChildren().addAll(rEdge,t);
    }
    
    // UTILIZO ESTA PARA GRAFICAR
    public EdgeGrafico(E data){
        t.setText(data.toString());
        t.setFill(Color.WHITE);
        t.setStyle("-fx-font-weight: bold");
        rEdge.setFill(javafx.scene.paint.Color.PURPLE);
        
        this.getChildren().addAll(rEdge,t);
    }

    public Rectangle getrEdge() {
        return rEdge;
    }

    public void setrEdge(Rectangle rEdge) {
        this.rEdge = rEdge;
    }
    
    public static double getCenter(){
        return sizes.getX()/2;
    }
    
}

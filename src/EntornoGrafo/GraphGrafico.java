/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntornoGrafo;

import Dataset.Archivo;
import Entidades.Actor;
import Entidades.Movie;
import GraphLA.GraphLA;
import GraphLA.Vertex;
import java.util.Iterator;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.TextFields;

/**
 *
 * @author scarlet Espinoza
 */
public class GraphGrafico {

    protected Pane pane = new Pane();
    protected VBox root = new VBox(40);
    protected Text titulo = new Text("The oracle of Actors");
    protected GraphLA<Actor> grafo;
    protected List<Vertex<Actor>> vertex;
    protected double xInicial = 700;
    protected double yInicial = 0;
    protected TextField TxtActorOrigen = new TextField();
    protected Text TxtTo = new Text("To");
    protected TextField TxtActorDestino = new TextField();
    protected Button BuscarBtn = new Button(" Buscar ");
    protected HBox BuscarHbox = new HBox(10);
    protected HBox GeneralHbox = new HBox(20);
    protected HBox HboxTitulo = new HBox();
    protected VBox vacio = new VBox();
    protected TextArea area = new TextArea();
    protected ScrollPane scroll = new ScrollPane();
    private Scene scene;
    
    public GraphGrafico() {
        this.grafo = new GraphLA<>(false);
        addActoresVertesGrafo();
        addPeliculasEdegesGrafo();
    }
    public GraphLA<Actor> getGrafo(){
        return grafo;
    }
    public Scene onPane() {
        BuscarBtn.setLayoutX(0);
        BuscarBtn.setLayoutY(600);
        TxtActorOrigen.setLayoutX(100);
        TxtActorOrigen.setLayoutY(600);
        TxtActorDestino.setLayoutX(100);
        TxtActorDestino.setLayoutY(600);
        TxtTo.setLayoutX(100);
        TxtTo.setLayoutY(600);
        TxtTo.setStyle("-fx-font-weight: bold");
        //pane.setPrefSize(1400, 700);
        scroll.setPannable(true);
        scroll.setContent(pane);
        scroll.setStyle("-fx-padding: 10px");
        titulo.setFill(Color.WHITE);
        titulo.setFont(Font.loadFont("file:fuente/Mont-Heavy.otf", 55));
        TxtTo.setFill(Color.WHITE);
        HboxTitulo.setAlignment(Pos.CENTER);
        HboxTitulo.setPrefSize(100,100);
        BuscarBtn.setTextFill(Color.BLACK);
        GeneralHbox.setAlignment(Pos.CENTER);
        
        BuscarBtn.setOnAction(e -> {
            PrintGraph printBFS = new PrintGraph(grafo, "BFS", 1);
            printBFS.setNombres(TxtActorOrigen.getText(), TxtActorDestino.getText());
            loadOnOtherStage(printBFS.onPane());
            
            PrintGraph printDFS = new PrintGraph(grafo, "DFS", 2);
            printDFS.setNombres(TxtActorOrigen.getText(), TxtActorDestino.getText());
            loadOnOtherStage(printDFS.onPane());
            
            PrintGraph printDijkstra = new PrintGraph(grafo, "Dijkstra", 3);
            printDijkstra.setNombres(TxtActorOrigen.getText(), TxtActorDestino.getText());
            loadOnOtherStage(printDijkstra.onPane());
        });
       
        BuscarHbox.getChildren().addAll(TxtActorOrigen, TxtTo, TxtActorDestino, BuscarBtn);
        GeneralHbox.getChildren().addAll(BuscarHbox);
        HboxTitulo.getChildren().add(titulo);
        root.getChildren().addAll(vacio, HboxTitulo, GeneralHbox);
        root.setStyle("-fx-background-color:#000000  ;");
        scene = new Scene(root, 700, 350);
        return scene;

    }
    
    //Walter
    public void setAuto() { //Es para que el textfield de actores pueda tener la funcion de auto completar
        TextFields.bindAutoCompletion(TxtActorDestino, grafo.getVertexes());
        TextFields.bindAutoCompletion(TxtActorOrigen, grafo.getVertexes());
    }
    
    
    //Walter
    public void setVertex() { //setea los vertices de; grafo
        for (Iterator<Actor> it = Archivo.getActores().values().iterator(); it.hasNext();) {
            this.grafo.addVertex(it.next());
        }
 
    }
    
    
    public  boolean oracleOfBacon(String actorD, String actorO){
        for(Actor a: Archivo.getActores().values()){
            if(a.getName().equals(actorD)){
                grafo.SearchWayMovie(a,BuscarActor(actorO));
                return true;
            }
        }
        System.out.println("Error actor no existe");
        return false;
    }
    
    public  Actor BuscarActor (String actor){
        for(Actor a: Archivo.getActores().values()){
            if(a.getName().equals(actor)){
                return a;
            }
        }
        System.out.println("Error actor no existe");
        return null;
    }
   
     public void addActoresVertesGrafo(){
        for(Actor a:Archivo.getActores().values()){
            grafo.addVertex(a);
        }
    }
     
     public void addPeliculasEdegesGrafo(){
        for(Actor actorOrigen:Archivo.getActores().values()){
            for(Movie p:actorOrigen.getListMovies()){
                for(Integer i:Archivo.getRelaciones().get(p.getIndexMovie())){
                    Actor actordestino = Archivo.getActores().get(i);
                    if(!actorOrigen.equals(actordestino)){
                        grafo.addEdge(actorOrigen, actordestino, p);
                    }
                    
                }
            }
        }
    }
     
    private void loadOnOtherStage(Scene scene){
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();     
    }

}

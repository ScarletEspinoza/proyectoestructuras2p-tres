/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntornoGrafo;

import Dataset.Archivo;
import Entidades.Actor;
import GraphLA.GraphLA;
import GraphLA.Vertex;
import java.util.List;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;

/**
 *
 * @author xavic
 */
public class PrintGraph {
    private VBox root;
    private Label titulo;
    protected Pane pane = new Pane();
    protected GraphLA<Actor> grafo;
    protected List<Vertex<Actor>> vertex;
    protected double xInicial = 700;
    protected double yInicial = 0;
    protected ScrollPane scroll = new ScrollPane();
    private Scene scene;
    private int tipoBusqueda;
    private String nombreOrigen;
    private String nombreDestino;
    
    public PrintGraph(GraphLA<Actor> grafo, String titulo, int tipoBusqueda) {
        this.grafo = grafo;
        this.titulo = new Label(titulo);
        this.tipoBusqueda = tipoBusqueda;
    }
    
    public void setNombres(String nombreOrigen, String nombreDestino){
        this.nombreOrigen = nombreOrigen;
        this.nombreDestino = nombreDestino;
    }
    
    public GraphLA<Actor> getGrafo(){
        return grafo;
    }
    
    public Scene onPane(){
        root = new VBox();
        scroll.setStyle("-fx-padding: 15px");
        scroll.setPannable(true);
        scroll.setContent(pane);
        scroll.setBorder(Border.EMPTY);
        
        titulo.setFont(Font.loadFont("file:fuente/Mont-Heavy.otf", 55));
        titulo.setStyle("-fx-padding: 15px");
        titulo.setStyle("-fx-text-fill:#FFFFFF;");
        root.setAlignment(Pos.CENTER);
        root.getChildren().addAll(titulo, scroll);
        root.setStyle("-fx-background-color: #000000 ;");
        scene = new Scene(root, 1200, 500);
        
        escogerBusqueda(tipoBusqueda);
        
        return scene;
    }
    
    // DECICE CUAL BUSQUEDA VA A MOSTRAR POR PANTALLA, SEGUN SE PIDE EN EL CONSTRUCTOR
    private int escogerBusqueda(int tipo){
        switch (tipo){
            case 1:
                oreacleOfActors(nombreOrigen, nombreDestino, 1);
                break;
            case 2:
                oreacleOfActors(nombreOrigen, nombreDestino, 2);
                break;
            case 3:
                oreacleOfActors(nombreOrigen, nombreDestino, 3);
                break;
        }
        return 0;
    }
    
//  REALIZA LA BUSQUEDA CON BFS, UTILIZA LA FUNCION PRIVADA CREATEVERTEXSANDEDGES
    public boolean oreacleOfActors(String actorO, String actorD, int tipoBusqueda){
        if(actorO.equals("")|| actorD.equals("")) return false;
        Actor origen = BuscarActor(actorO);
        Actor destino = BuscarActor(actorD);
        if(origen==null || destino==null) return false;
        List<List<String>> lista = grafo.searchWayMovie(origen, destino, tipoBusqueda);
        List<String> actores = lista.get(0);
        List<String> peliculas = lista.get(1);
        createVertexsAndEdges(actores, peliculas);
        return true;
    }
    
    // CREA LOS VERTICES Y EDGES QUE SE PRESENTAN
    private void createVertexsAndEdges(List<String> names, List<String> movies){
        pane.getChildren().clear();
        double differenceXVertex = VertexGrafico.getCenter();
        double differenceXEdge = EdgeGrafico.getCenter();
        
        Point2D vertexPosition = new Point2D(scene.getWidth()/2-differenceXVertex, 0);
        Point2D edgePosition = new Point2D(scene.getWidth()/2-differenceXEdge, 100);
        Line lineActorPelicula = new Line(vertexPosition.getX()+differenceXVertex, vertexPosition.getY(), edgePosition.getX()+differenceXEdge, edgePosition.getY());
        Line linePeliculaActor = new Line(vertexPosition.getX()+differenceXVertex, vertexPosition.getY()+200, edgePosition.getX()+differenceXEdge, edgePosition.getY());
        
        for (int i = 0; i < names.size(); i++) { 
            VertexGrafico<String> v = new VertexGrafico<>(names.get(i));
            v.setLayoutX(vertexPosition.getX());
            v.setLayoutY(vertexPosition.getY());
        
            vertexPosition = new Point2D(vertexPosition.getX(), vertexPosition.getY()+200);
            
            if(i < movies.size()){
                EdgeGrafico<String> e = new EdgeGrafico<>(movies.get(i));
                e.setLayoutX(edgePosition.getX());
                e.setLayoutY(edgePosition.getY());

                edgePosition = new Point2D(edgePosition.getX(), edgePosition.getY()+200);
                
                pane.getChildren().add(linePeliculaActor);
                
                linePeliculaActor = new Line(vertexPosition.getX()+differenceXVertex, vertexPosition.getY()+200, edgePosition.getX()+differenceXEdge, edgePosition.getY()); 
                
                pane.getChildren().add(lineActorPelicula);
            
                lineActorPelicula = new Line(vertexPosition.getX()+differenceXVertex, vertexPosition.getY(), edgePosition.getX()+differenceXEdge, edgePosition.getY());
            
                pane.getChildren().add(e);
            }
            
            pane.getChildren().add(v);
        }       
    }
    
    
    public  Actor BuscarActor (String actor){
        for(Actor a: Archivo.getActores().values()){
            if(a.getName().equals(actor)){
                return a;
            }
        }
        System.out.println("Error actor no existe");
        return null;
    }
}

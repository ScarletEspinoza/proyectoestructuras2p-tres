/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author scarlet Espinoza
 */
public class Movie {
    private int indexMovie;
    private String nameMove;

    public Movie(int indexMovie, String nameMove) {
        this.indexMovie = indexMovie;
        this.nameMove = nameMove;
    }

    public int getIndexMovie() {
        return indexMovie;
    }

    public void setIndexMovie(int indexMovie) {
        this.indexMovie = indexMovie;
    }

    public String getNameMove() {
        return nameMove;
    }

    public void setNameMove(String nameMove) {
        this.nameMove = nameMove;
    }

    @Override
    public String toString() {
        return nameMove;
    }
    
}

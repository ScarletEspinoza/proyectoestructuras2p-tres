/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author scarlet Espinoza
 */
public class Actor {
    private int indexActor;
    private String nameActor;
    private List<Movie> listMovies = new LinkedList<>();

    public Actor(int numero, String name) {
        this.indexActor = numero;
        this.nameActor = name;
    }

    public int getNumero() {
        return indexActor;
    }

    public void setNumero(int numero) {
        this.indexActor = numero;
    }

    public String getName() {
        return nameActor;
    }

    public void setName(String name) {
        this.nameActor = name;
    }

    public List<Movie> getListMovies() {
        return listMovies;
    }

    public void setListMovies(List<Movie> listMovies) {
        this.listMovies = listMovies;
    }
    
    public String actuaEnPeliculaCon(Actor actor){
        for(Movie movie : listMovies){
            for(Movie mov : actor.getListMovies()){
                if(movie.getIndexMovie() == mov.getIndexMovie())
                    return movie.getNameMove();
            }
        }
        return "No encontrado";
    }

    @Override
    public String toString() {
        return nameActor;
    }
    
    
    
}

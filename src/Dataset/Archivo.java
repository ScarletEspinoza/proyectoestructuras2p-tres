/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dataset;

import Entidades.Actor;
import Entidades.Movie;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author walte
 */
public class Archivo {
    private static Map<Integer,Actor> actores = new HashMap<>(); //mapa de actores, las claves son el id
    private static Map<Integer,Movie> peliculas = new HashMap<>(); //mapa de peliculas, las claves son el id
    private static Map<Integer,List<Integer>> relaciones = new HashMap<>(); //Queue de relaciones para crear el grafo
   
    
    public static void leerActores(){ //lee el archivo de actores
        File file = new File("src/Dataset/actor.txt");
        try(Scanner sc = new Scanner(file)) {            
            while(sc.hasNextLine()){
                String result = sc.nextLine();
                String[] resultList = result.split("\\|");
                actores.putIfAbsent(Integer.parseInt(resultList[0]), new Actor(Integer.parseInt(resultList[0]),resultList[1]));
               
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado.");
        }
       
    }
    
    public static void leerPeliculas(){ //lee el archivo de peliculas
        File file = new File("src/Dataset/movies.txt");
        try(Scanner sc = new Scanner(file)) {
            while(sc.hasNextLine()){
                String[] result = sc.nextLine().split("\\|");
                peliculas.putIfAbsent(Integer.parseInt(result[0]), new Movie(Integer.parseInt(result[0]),result[1]));
                
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado.");
        }
       
    }
    public static void leerRelaciones(){ //lee el archivo de relaciones que esta conformado por pelicula|actor
        File file = new File("src/Dataset/Relationships.txt");
        try(Scanner sc = new Scanner(file)) {
            while(sc.hasNextLine()){
                String[] result = sc.nextLine().split("\\|");
                 relaciones.putIfAbsent(Integer.parseInt(result[0]), new LinkedList<>());
                            relaciones.get(Integer.parseInt(result[0])).add(Integer.parseInt(result[1]));
                            if(actores.containsKey(Integer.parseInt(result[1]))){
                                actores.get(Integer.parseInt(result[1])).getListMovies().add(peliculas.get(Integer.parseInt(result[0])));
                            }
                
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado.");
        }
    }

    public static Map<Integer, Actor> getActores() {
        return actores;
    }

    public static Map<Integer, Movie> getPeliculas() {
        return peliculas;
    }

    public static Map<Integer, List<Integer>> getRelaciones() {
        return relaciones;
    }
 
}

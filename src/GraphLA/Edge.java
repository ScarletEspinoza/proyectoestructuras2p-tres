/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GraphLA;

import Entidades.Movie;
import java.util.Objects;

/**
 *
 * @author scarlet Espinoza
 */
public class Edge<E> {
    private Vertex<E> origen;
    private Vertex<E> destino;
    private int peso;
    private Movie movie;
    public Edge(Vertex<E> origen, Vertex<E> destino, Movie movie) {
        this.origen = origen;
        this.destino = destino;
        this.peso =1;
        this.movie = movie;
    }

    public Vertex<E> getOrigen() {
        return origen;
    }

    public void setOrigen(Vertex<E> origen) {
        this.origen = origen;
    }

    public Vertex<E> getDestino() {
        return destino;
    }

    public void setDestino(Vertex<E> destino) {
        this.destino = destino;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.origen.getData());
        hash = 89 * hash + Objects.hashCode(this.destino.getData());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Edge<E> other = (Edge<E>) obj;
        return other.getDestino().equals(this.getDestino()) && other.getOrigen().equals(this.getOrigen()); 
    }

     @Override
    public String toString() {
        return  movie.toString();
    }

}

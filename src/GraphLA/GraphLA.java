/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GraphLA;

import Entidades.Actor;
import Entidades.Movie;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

/**
 *
 * @author scarlet Espinoza
 */
public class GraphLA<E> {
    private boolean directed;
    private List<Vertex<E>> vertexes;
    
    public GraphLA(boolean directed){
        vertexes = new LinkedList<>();
        this.directed=directed;
   }
    
    public boolean addVertex(E data){
        Vertex<E> v = new Vertex<>(data);
        if(data==null || this.vertexes.contains(v))
            return false;
        return this.vertexes.add(v);
    }
    
    public boolean addEdge(E origen,E destino, Movie m){
        if(origen == null || destino == null )
            return false;
        Vertex<E> vo = searchVertex(origen);
        Vertex<E> vd = searchVertex(destino);
        if(vo==null || vd==null)
            return false;
        Edge<E>  e = new Edge<>(vo,vd,m);
        if(vo.getEdges().contains(e))
            return false;
        vo.getEdges().add(e);
        if(!directed){
            Edge<E> e1 = new Edge<>(vd,vo,m);
            vd.getEdges().add(e1);   
        }
        return true;
    }
    
    
    private Vertex<E> searchVertex(E data){
        for(Vertex<E> v: this.vertexes){
            if(v.getData().equals(data))
                return v;
        }
        return null;
    }
    
    
    public boolean removeVertex(E data){
        if(data==null) return false;
        Vertex<E> v = searchVertex(data);
        if(v==null) return false;
        ListIterator<Vertex<E>> itv = vertexes.listIterator();
        while(itv.hasNext()){
            Vertex<E> v1 = itv.next();
            ListIterator<Edge<E>> ite = v1.getEdges().listIterator();
            while(ite.hasNext()){
                Edge<E> e1 = ite.next();
                if(e1.getDestino().getData().equals(data)){
                    ite.remove();
                }
            }
        }
        v.getEdges().clear();
        return vertexes.remove(v);
    
    }
    
    public int indregree(E data){  
        if(data==null)  return 0;
        int cont=0;
        for(Vertex<E> v1:vertexes){
            for(Edge<E> e:v1.getEdges()){
                if(e.getDestino().getData().equals(data))
                    cont++;
            }
        }
        return cont;
    }
    
    public int outdegree(E data){
        Vertex<E> v = searchVertex(data);
        if(v== null)    return 0;
        return v.getEdges().size();
        
    }
    
     public List<E> bfs(E origen){
        List<E> resultado = new LinkedList<>();
        Queue<Vertex<E>> cola = new LinkedList<>();
        Vertex<E> v = searchVertex(origen);
        if(v==null) return resultado;
        v.setVisited(true);
        cola.offer(v);
        while(!cola.isEmpty()){
            v = cola.poll();
            resultado.add(v.getData());
            for(Edge<E> e:v.getEdges()){
                if(!e.getDestino().isVisited()){
                    e.getDestino().setVisited(true);
                    e.getDestino().setAntecesor(v); //AGREGADO PARA EL PROYECTO
                    cola.offer(e.getDestino());
                }
            }
        }
        //cleanVertex();
        return resultado;
        
    }
     public List<E> dfs(E data){
        List<E> lista = new LinkedList<>();
        Vertex<E> v = searchVertex(data);
        if(v==null || this.isEmpty()){
            return lista;
        }
        Stack<Vertex<E>> pila = new Stack<>();
        v.setVisited(true);
        pila.push(v);
        while(!pila.isEmpty()){
            Vertex<E> vi = pila.pop();
            lista.add(vi.getData());
            for(Edge<E> e:vi.getEdges()){
                if(!e.getDestino().isVisited()){
                    e.getDestino().setVisited(true);
                    e.getDestino().setAntecesor(vi); //AGREGADO PARA EL PROYECTO
                    pila.push(e.getDestino());
                }
            }
        }
        //cleanVertex();
        return lista;
    }
     
      public void cleanVertex(){
        for(Vertex<E> v:vertexes){
            v.setVisited(false);
            v.setDistancia(Integer.MAX_VALUE);
            v.setAntecesor(null);
        }
        
    }
       private void dijkstra(E origen){
        Vertex<E> vo = searchVertex(origen);
        if(vo != null){
            vo.setDistancia(0);
            PriorityQueue<Vertex<E>> cola = new PriorityQueue<>((Vertex<E> v1,Vertex<E> v2)-> v1.getDistancia()-v2.getDistancia());
            cola.offer(vo);
            while(!cola.isEmpty()){
                Vertex<E> fin = cola.poll();
                fin.setVisited(true);
                for(Edge<E> e:fin.getEdges()){
                    if(fin.getDistancia()+e.getPeso()<e.getDestino().getDistancia() && !e.getDestino().isVisited()){
                        e.getDestino().setDistancia(fin.getDistancia()+e.getPeso());
                        e.getDestino().setAntecesor(fin);                                
                        cola.offer(e.getDestino());
                    }
                }
            }
        }
    }  
        
    public boolean isDirected() {
        return directed;
    }

    public void setDirected(boolean directed) {
        this.directed = directed;
    }

    public List<Vertex<E>> getVertexes() {
        return vertexes;
    }

    public void setVertexes(List<Vertex<E>> vertexes) {
        this.vertexes = vertexes;
    }
    
    @Override
    public boolean equals(Object o){
        if(!(o instanceof GraphLA))
            return false;
        GraphLA<E> graph = (GraphLA<E>)o;
        
        if(this.vertexes.size()!= graph.vertexes.size())
            return false;
        HashSet<Vertex<E>> s1 = new HashSet<>();
        HashSet<Vertex<E>> s2 = new HashSet<>();
        s1.addAll(this.vertexes);
        s2.addAll(graph.vertexes);
        s1.removeAll(s2);
        if(!s1.isEmpty())
            return false;
        for(Vertex<E> v1:this.vertexes){
            Vertex<E> v2 = graph.searchVertex(v1.getData());
            HashSet<Edge<E>> e1 = new HashSet<>();
            HashSet<Edge<E>> e2 = new HashSet<>();
            e1.addAll(v1.getEdges());
            e2.addAll(v2.getEdges());
            e1.removeAll(e2);
            if(!e1.isEmpty())
                return false;
        }
        return true;
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.vertexes);
        return hash;
    }
    //Walter
     @Override
    public String toString() { //to string del grafoLA
        StringBuilder sb = new StringBuilder();
        StringBuilder vertex = new StringBuilder();
        StringBuilder edges = new StringBuilder();
        vertex.append("V:");
        edges.append("A:");
        edges.append("[");
        vertex.append(vertexes);
        for(Vertex<E> v: vertexes){
            
            for(Edge<E> e: v.getEdges()){
                edges.append(e);
            }
        }
        edges.append("]");
      
        sb.append(vertex);
        sb.append("\n");
        sb.append(edges);
        return sb.toString();
    }
    
    public boolean isEmpty(){
        return vertexes.isEmpty();
    }
    
    public void SearchWayMovie(E destino,E origen){
        cleanVertex();
        int numero = 0;
        dijkstra(origen);
        Vertex<E> vo = searchVertex(destino);
        if(vo!=null){
            while(vo.getAntecesor()!=null){
                System.out.println(vo);
                System.out.println(vo.searchEdge());
                vo = vo.getAntecesor();
                numero++;
            }
        }
        System.out.println("Origen Actor:"+origen);
        System.out.println("Numero: "+numero);
    }
    
    // TRUE PARA BUSQUEDA BFS, FALSE PARA DFS
    // LISTA DE LISTAS, SOLO CONTIENE 2 LISTAS
    // LA PRIMERA TIENE LA LISTA CON NOMBRES DE ACTORES
    // LA SEGUNDA TIENE LA LISTA CON NOMBRE DE LAS PELICULAS
    public List<List<String>> printWayBFSorDFS(E origen, E destino, boolean bfs){
        cleanVertex();
        // seteando las lista para devolver
        List<List<String>> lista = new ArrayList<>();
        List<String> nombreActores = new ArrayList<>();
        List<String> nombrePeliculas = new ArrayList<>();
        
        // seteando valores para utilizar busqueda bfs o dfs
        int numero = 0;
        if(bfs)
            bfs(origen);
        else
            dfs(origen);
        //int indexDestino = vertices.indexOf(destino);
        Vertex<E> vd = searchVertex(destino);
        if(vd!=null){
            while(vd.getAntecesor()!=null){
                System.out.println(vd);
                Actor actor = (Actor) vd.getData();
                nombreActores.add(actor.getName());
                
                System.out.println(vd.searchEdge());
                Edge edge = (Edge) vd.searchEdge();
                nombrePeliculas.add(edge.getMovie().getNameMove());
                
                vd = vd.getAntecesor();
                numero++;
            }
        }
        System.out.println("Origen Actor:"+origen);
        Actor actorOrigen = (Actor) origen;
        nombreActores.add(actorOrigen.getName());
        
        System.out.println("Numero: "+numero);
        
        lista.add(nombreActores);
        lista.add(nombrePeliculas);
        
        return lista;
    }
    
    // METODO QUE BUSCA SEGUN EL TIPO QUE SE SOLICITA
    // 1 PARA BFS - 2 PARA DFS - 3 PARA DIJKSTRA
    public List<List<String>> searchWayMovie(E origen, E destino, int tipoBusqueda){
        cleanVertex();
        // seteando las lista para devolver
        List<List<String>> lista = new ArrayList<>();
        List<String> nombreActores = new ArrayList<>();
        List<String> nombrePeliculas = new ArrayList<>();
        // TIEMPO DONDE EMPIEZA EL PROCESO
        long startTime = System.currentTimeMillis();
        String proceso = null;
        // seteando valores para utilizar busqueda bfs, dfs o dijkstra
        switch(tipoBusqueda){
            case 1:
                bfs(origen);
                proceso = "El proceso para la busqueda BFS tomó: ";
                break;
            case 2:
                dfs(origen);
                proceso = "El proceso para la busqueda DFS tomó: ";
                break;
            case 3:
                dijkstra(origen);
                proceso = "El proceso para la busqueda DIJKSTRA tomó: ";
                break;
        }
        Vertex<E> vd = searchVertex(destino);
        if(vd!=null){
            while(vd.getAntecesor()!=null){
                Actor actor = (Actor) vd.getData();
                nombreActores.add(actor.getName());
                
                Edge edge = (Edge) vd.searchEdge();
                nombrePeliculas.add(edge.getMovie().getNameMove());
                
                vd = vd.getAntecesor();
            }
        }
        Actor actorOrigen = (Actor) origen;
        nombreActores.add(actorOrigen.getName());
        
        lista.add(nombreActores);
        lista.add(nombrePeliculas);
        // TIEMPO DONDE ACABA EL PROCESO
        long endTime = System.currentTimeMillis() - startTime;
        
        System.out.println(proceso + endTime + " milisegundos");
        
        return lista;
    }
}
    
    
    
    
    
    
    
      
    

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Dataset.Archivo;
import EntornoGrafo.GraphGrafico;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 *
 * @author scarlet Espinoza
 */
public class main extends Application  {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
         launch();
    }
    
   
    public void start(Stage stage) {
        Archivo.leerActores(); //crea el mapa de actores
        Archivo.leerPeliculas(); //crea el mapa de peliculas
        Archivo.leerRelaciones(); //crea el mapa de relaciones
        stage.setTitle("Oracle of actors");
        GraphGrafico grafoGrafico = new GraphGrafico();
        grafoGrafico.setVertex();
        Scene s = grafoGrafico.onPane();
        stage.setScene(s);
        stage.show();
        grafoGrafico.setAuto();

    }

}
